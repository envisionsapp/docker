
docker build --compress -t registry.gitlab.com/envisionsapp/docker:$TAG .

docker login -u docker-0 --password-stdin registry.gitlab.com
docker push registry.gitlab.com/envisionsapp/docker:$TAG
